from . import config
import io
import os
from PIL import Image, ImageCms
from PyPDF2 import PdfReader, PdfWriter, PdfMerger
from PyPDF2 import PageObject
import re
import subprocess
import tempfile


def escape_tex(text):
    '''
    Input:
    * ``: () ...

    Output:
    * () ...

    Function:
    * ...
    '''

    conv = {
        '&': r'\&',
        '%': r'\%',
        '$': r'\$',
        '#': r'\#',
        '_': r'\_',
        '{': r'\{',
        '}': r'\}',
        '~': r'\textasciitilde{}',
        '^': r'\^{}',
        '\\': r'\textbackslash{}',
        '<': r'\textless{}',
        '>': r'\textgreater{}',
    }
    regex = re.compile('|'.join(re.escape(
        # unicode(key)
        key
    ) for key in sorted(conv.keys(), key=lambda item: - len(item))))
    return regex.sub(lambda match: conv[match.group()], str(text))


def prepare_tex_text(text, tex_root_path, text_separator_file_name):
    '''
    Input:
    * ``: () ...

    Output:
    * () ...

    Function:
    * ...
    '''

    return get_tex_content(
        tex_root_path,
        text_separator_file_name
    ).strip().join(
        map(
            escape_tex,
            filter(
                None,
                [line.strip() for line in text.splitlines()]
            )
        )
    )


def get_tex_content(
    tex_root_path,
    tex_file_name,
    escape_tex_exclude=[],
    **kwargs
):
    '''
    Input:
    * ``: () ...

    Output:
    * () ...

    Function:
    * ...
    '''

    tex_path = os.path.join(
        tex_root_path,
        tex_file_name
    )

    with open(tex_path, 'r') as tex_object:
        tex_content = tex_object.read()

    for (key, value) in kwargs.items():
        tex_content = tex_content.replace(
            key,
            key in escape_tex_exclude and value or escape_tex(value)
        )

    return tex_content


def get_tmp_tex(
    tex_content,
    tex_tmp_path
):
    '''
    Input:
    * ``: () ...

    Output:
    * () ...

    Function:
    * ...
    '''

    with NamedTemporaryFile(
        mode='w',
        dir=tex_tmp_path,
        delete=False
    ) as tmp_tex_object:
        tmp_tex_object.write(tex_content)

    return tmp_tex_object.name


def delete_tmp_tex(tmp_tex_path):
    '''
    Input:
    * ``: () ...

    Output:
    * () ...

    Function:
    * ...
    '''

    for extension in ('', '.aux', '.log', '.pdf'):
        os.remove('{}{}'.format(tmp_tex_path, extension))


def compile_tex(tex_content, tex_tmp_path='/tmp'):
    '''
    Input:
    * ``: () ...

    Output:
    * () ...

    Function:
    * ...
    '''

    with tempfile.TemporaryDirectory(
        dir=tex_tmp_path
    ) as temporary_directory:

        with tempfile.NamedTemporaryFile(
            mode='w',
            dir=temporary_directory,
            delete=False
        ) as named_temporary_file:
            named_temporary_file.write(tex_content)

        named_temporary_file_path = os.path.join(
            temporary_directory,
            named_temporary_file.name
        )

        completed_process = subprocess.run(
            [
                'xelatex',
                '-output-directory',
                temporary_directory,
                named_temporary_file_path
            ],
            stdout=subprocess.PIPE
        )

        if completed_process.returncode == 0:
            with open('{}{}'.format(
                named_temporary_file_path, '.pdf'
            ), 'rb') as pdf_object:
                pdf_bytes = io.BytesIO(pdf_object.read())
            return pdf_bytes
        else:
            raise Exception(completed_process.stdout.decode('utf-8').strip())


def merge_pdfs(*pdf_objects):
    """
    Input:
    * ``: () ...

    Output:
    * () ...

    Function:
    * ...
    """

    merger = PdfMerger()
    for pdf_object in pdf_objects:
        merger.append(PdfReader(pdf_object, strict=False))
    pdf_bytes = io.BytesIO()
    merger.write(pdf_bytes)
    return pdf_bytes


def get_pdf_number_of_pages(pdf_object):
    """
    Input:
    * ``: () ...

    Output:
    * () ...

    Function:
    * ...
    """
    reader=PdfReader(pdf_object, strict=False)
    return len(reader.pages)


def merge_pdfs_pages(pdf_object_1, pdf_object_2):
    """
    Input:
    * ``: () ...

    Output:
    * () ...

    Function:
    * ...
    """

    writer = PdfWriter()
    reader_1 = PdfReader(pdf_object_1, strict=False)
    reader_2 = PdfReader(pdf_object_2, strict=False)
    for page_number in range(len(reader_1.pages)):
        page_1 = reader_1.pages[page_number]
        page_2 = reader_2.pages[page_number]
        page_1.merge_page(page_2)
        writer.add_page(page_1)
    pdf_bytes = io.BytesIO()
    writer.write(pdf_bytes)
    return pdf_bytes


def rotate_pdf_90_counterclockwise(pdf_object):
    """
    Input:
    * ``: () ...

    Output:
    * () ...

    Function:
    * ...
    """

    writer = PdfWriter()
    reader = PdfReader(pdf_object, strict=False)
    for page_number in range(len(reader.pages)):
        page = reader.pages[page_number]
        page.add_transformation([0, 1, -1, 0, page.mediabox.height, 0])
        page.mediabox.upper_right=reversed(page.mediabox.upper_right)
        writer.add_page(page)
    pdf_bytes = io.BytesIO()
    writer.write(pdf_bytes)
    return pdf_bytes


def get_pdf_blank_page(orientation="Portrait"):
    """
    Input:
    * ``: () ...

    Output:
    * () ...

    Function:
    * ...
    """

    if orientation == "Portrait":
        A4_WIDTH = 595
        A4_HEIGHT = 842
    elif orientation == "Landscape":
        A4_WIDTH = 842
        A4_HEIGHT = 595

    BLANK_PAGE_BYTES = io.BytesIO(b"%PDF-1.5\n%\xb5\xed\xae\xfb\n3 0 obj\n<< /Length 4 0 R\n   /Filter /FlateDecode\n>>\nstream\nx\x9c+\xe42T\x00\xc1\xa2t\x05\xfdD\x03\x85\xf4b.\x03\x05\x03\x05c#\x10*JUH\xe3\n\xe4\x02\x00\x8a\x08\x07\x98\nendstream\nendobj\n4 0 obj\n   38\nendobj\n2 0 obj\n<<\n   /ExtGState <<\n      /a0 << /CA 1 /ca 1 >>\n   >>\n>>\nendobj\n5 0 obj\n<< /Type /Page\n   /Parent 1 0 R\n   /MediaBox [ 0 0 32 32 ]\n   /Contents 3 0 R\n   /Group <<\n      /Type /Group\n      /S /Transparency\n      /I true\n      /CS /DeviceRGB\n   >>\n   /Resources 2 0 R\n>>\nendobj\n1 0 obj\n<< /Type /Pages\n   /Kids [ 5 0 R ]\n   /Count 1\n>>\nendobj\n6 0 obj\n<< /Creator (cairo 1.14.0 (http://cairographics.org))\n   /Producer (cairo 1.14.0 (http://cairographics.org))\n>>\nendobj\n7 0 obj\n<< /Type /Catalog\n   /Pages 1 0 R\n>>\nendobj\nxref\n0 8\n0000000000 65535 f \n0000000435 00000 n \n0000000151 00000 n \n0000000015 00000 n \n0000000130 00000 n \n0000000223 00000 n \n0000000500 00000 n \n0000000627 00000 n \ntrailer\n<< /Size 8\n   /Root 7 0 R\n   /Info 6 0 R\n>>\nstartxref\n679\n%%EOF\n")

    blank_page_object = PdfReader(BLANK_PAGE_BYTES).pages[0]
    blank_page_object.scale_to(
        width=A4_WIDTH,
        height=A4_HEIGHT
    )

    writer = PdfWriter()
    writer.add_page(blank_page_object)

    pdf_bytes = io.BytesIO()
    writer.write(pdf_bytes)
    return pdf_bytes


def n_up(
    slides_object,
    n=4,
    margin_top=40,
    margin_right=40,
    margin_bottom=40,
    margin_left=40,
    margin_horizontal=20,
    margin_vertical=20,
    border_size=0.5,
    expand=False
):
    '''
    Input:
    * ``: () ...

    Output:
    * () ...

    Function:
    * ...
    '''

    N = n

    A4_WIDTH = 842
    A4_HEIGHT = 595

    MARGIN_TOP = margin_top
    MARGIN_RIGHT = margin_right
    MARGIN_BOTTOM = margin_bottom
    MARGIN_LEFT = margin_left

    PAGE_WIDTH = A4_WIDTH - MARGIN_LEFT - MARGIN_RIGHT
    PAGE_HEIGHT = A4_HEIGHT - MARGIN_TOP - MARGIN_BOTTOM
    PAGE_RATIO = PAGE_WIDTH / PAGE_HEIGHT

    MARGIN_HORIZONTAL = margin_horizontal
    MARGIN_VERTICAL = margin_vertical

    BORDER_SIZE = border_size

    BORDER_BYTES = io.BytesIO(b'%PDF-1.5\n%\xb5\xed\xae\xfb\n3 0 obj\n<< /Length 4 0 R\n   /Filter /FlateDecode\n>>\nstream\nx\x9c+\xe42P\x00\xc1\xa2t\x05\xfdD\x03\x85\xf4b0\xdf\xd8\x08\x84\x8aR\x15\xd2\xb8\x02\xb9\x00\x89\xab\x07\x95\nendstream\nendobj\n4 0 obj\n   34\nendobj\n2 0 obj\n<<\n   /ExtGState <<\n      /a0 << /CA 1 /ca 1 >>\n   >>\n>>\nendobj\n5 0 obj\n<< /Type /Page\n   /Parent 1 0 R\n   /MediaBox [ 0 0 32 32 ]\n   /Contents 3 0 R\n   /Group <<\n      /Type /Group\n      /S /Transparency\n      /I true\n      /CS /DeviceRGB\n   >>\n   /Resources 2 0 R\n>>\nendobj\n1 0 obj\n<< /Type /Pages\n   /Kids [ 5 0 R ]\n   /Count 1\n>>\nendobj\n6 0 obj\n<< /Creator (cairo 1.14.0 (http://cairographics.org))\n   /Producer (cairo 1.14.0 (http://cairographics.org))\n>>\nendobj\n7 0 obj\n<< /Type /Catalog\n   /Pages 1 0 R\n>>\nendobj\nxref\n0 8\n0000000000 65535 f \n0000000431 00000 n \n0000000147 00000 n \n0000000015 00000 n \n0000000126 00000 n \n0000000219 00000 n \n0000000496 00000 n \n0000000623 00000 n \ntrailer\n<< /Size 8\n   /Root 7 0 R\n   /Info 6 0 R\n>>\nstartxref\n675\n%%EOF\n')

    TRANSPARENCY_BYTES = io.BytesIO(b'%PDF-1.5\n%\xb5\xed\xae\xfb\n3 0 obj\n<< /Length 4 0 R\n   /Filter /FlateDecode\n>>\nstream\nx\x9c+\xe42T\x00\xc1\xa2t\x05\xfdD\x03\x85\xf4b.\x03\x05\x03\x05c#\x10*JUH\xe3\n\xe4\x02\x00\x8a\x08\x07\x98\nendstream\nendobj\n4 0 obj\n   38\nendobj\n2 0 obj\n<<\n   /ExtGState <<\n      /a0 << /CA 1 /ca 1 >>\n   >>\n>>\nendobj\n5 0 obj\n<< /Type /Page\n   /Parent 1 0 R\n   /MediaBox [ 0 0 32 32 ]\n   /Contents 3 0 R\n   /Group <<\n      /Type /Group\n      /S /Transparency\n      /I true\n      /CS /DeviceRGB\n   >>\n   /Resources 2 0 R\n>>\nendobj\n1 0 obj\n<< /Type /Pages\n   /Kids [ 5 0 R ]\n   /Count 1\n>>\nendobj\n6 0 obj\n<< /Creator (cairo 1.14.0 (http://cairographics.org))\n   /Producer (cairo 1.14.0 (http://cairographics.org))\n>>\nendobj\n7 0 obj\n<< /Type /Catalog\n   /Pages 1 0 R\n>>\nendobj\nxref\n0 8\n0000000000 65535 f \n0000000435 00000 n \n0000000151 00000 n \n0000000015 00000 n \n0000000130 00000 n \n0000000223 00000 n \n0000000500 00000 n \n0000000627 00000 n \ntrailer\n<< /Size 8\n   /Root 7 0 R\n   /Info 6 0 R\n>>\nstartxref\n679\n%%EOF\n')

    try:
        slides_reader = PdfReader(slides_object, strict=False)
    except Exception as e:
        raise Exception("Failed to read the PDF Slides: {}".format(e))

    number_of_slides = len(slides_reader.pages)

    slides_page_0 = slides_reader.pages[0]
    slides_width = float(slides_page_0.mediabox.width)
    slides_height = float(slides_page_0.mediabox.height)
    slides_ratio = slides_width / slides_height
    del(slides_page_0)

    if slides_ratio > PAGE_RATIO:

        slides_scaled_width = (
            PAGE_WIDTH - (
                4 * BORDER_SIZE
            ) - MARGIN_HORIZONTAL
        ) / 2

        slides_scaled_height = slides_scaled_width / slides_ratio

        border_width = slides_scaled_width + (2 * BORDER_SIZE)
        border_height = slides_scaled_height + (2 * BORDER_SIZE)

        MARGIN_LEFT_ADJUSTMENT = 0
        MARGIN_HORIZONTAL_ADJUSTMENT = 0

        if expand:
            MARGIN_BOTTOM_ADJUSTMENT = 0
            MARGIN_VERTICAL_ADJUSTMENT = PAGE_HEIGHT - (
                2 * border_height
            ) - MARGIN_VERTICAL
        else:
            MARGIN_BOTTOM_ADJUSTMENT = (PAGE_HEIGHT - (
                2 * border_height
            ) - MARGIN_VERTICAL) / 2
            MARGIN_VERTICAL_ADJUSTMENT = 0

    elif slides_ratio <= PAGE_RATIO:

        slides_scaled_height = (
            PAGE_HEIGHT - (
                4 * BORDER_SIZE
            ) - MARGIN_VERTICAL
        ) / 2

        slides_scaled_width = slides_scaled_height * slides_ratio

        border_height = slides_scaled_height + (2 * BORDER_SIZE)
        border_width = slides_scaled_width + (2 * BORDER_SIZE)

        MARGIN_BOTTOM_ADJUSTMENT = 0
        MARGIN_VERTICAL_ADJUSTMENT = 0

        if expand:
            MARGIN_LEFT_ADJUSTMENT = 0
            MARGIN_HORIZONTAL_ADJUSTMENT = PAGE_WIDTH - (
                2 * border_width
            ) - MARGIN_HORIZONTAL
        else:
            MARGIN_LEFT_ADJUSTMENT = (PAGE_WIDTH - (
                2 * border_width
            ) - MARGIN_HORIZONTAL) / 2
            MARGIN_HORIZONTAL_ADJUSTMENT = 0

    slides_translation = {
        0: {
            'x': MARGIN_LEFT + MARGIN_LEFT_ADJUSTMENT,
            'y': MARGIN_BOTTOM + (
                2 * BORDER_SIZE
            ) + slides_scaled_height + (
                MARGIN_VERTICAL
            ) + MARGIN_VERTICAL_ADJUSTMENT + (
                MARGIN_BOTTOM_ADJUSTMENT
            )
        },
        1: {
            'x': MARGIN_LEFT + (
                2 * BORDER_SIZE
            ) + slides_scaled_width + (
                MARGIN_HORIZONTAL
            ) + MARGIN_HORIZONTAL_ADJUSTMENT + (
                MARGIN_LEFT_ADJUSTMENT
            ),
            'y': MARGIN_BOTTOM + (
                2 * BORDER_SIZE
            ) + slides_scaled_height + (
                MARGIN_VERTICAL
            ) + MARGIN_VERTICAL_ADJUSTMENT + (
                MARGIN_BOTTOM_ADJUSTMENT
            )
        },
        2: {
            'x': MARGIN_LEFT + MARGIN_LEFT_ADJUSTMENT,
            'y': MARGIN_BOTTOM + MARGIN_BOTTOM_ADJUSTMENT
        },
        3: {
            'x': MARGIN_LEFT + (
                2 * BORDER_SIZE
            ) + slides_scaled_width + (
                MARGIN_HORIZONTAL
            ) + MARGIN_HORIZONTAL_ADJUSTMENT + (
                MARGIN_LEFT_ADJUSTMENT
            ),
            'y': MARGIN_BOTTOM + MARGIN_BOTTOM_ADJUSTMENT
        },
    }

    n_up_object = io.BytesIO()
    n_up_writer = PdfWriter()

    for number_of_slide in range(number_of_slides):

        number_of_slide_modulo = number_of_slide % N

        if number_of_slide_modulo == 0:
            page_object = PageObject.createBlankPage(
                width=A4_WIDTH,
                height=A4_HEIGHT
            )

        slide_object = slides_reader.pages[number_of_slide]
        try:
            slide_object.scale_to(
                width=slides_scaled_width,
                height=slides_scaled_height
            )
        except Exception as e:
            raise Exception("Failed to scale slide number {}: {}".format(
                number_of_slide + 1, e)
            )

        if '/Annots' in slide_object:
            del(slide_object['/Annots'])

        transparency_page_object = PdfReader(
            TRANSPARENCY_BYTES
        ).pages[0]
        transparency_page_object.scale_to(
            width=slides_scaled_width,
            height=slides_scaled_height
        )
        transparency_page_object.mergeTranslatedPage(
            page2=slide_object,
            tx=0,
            ty=0,
            expand=False
        )

        border_page_object = PdfReader(
            BORDER_BYTES
        ).pages[0]
        border_page_object.scale_to(
            width=border_width,
            height=border_height
        )
        border_page_object.mergeTranslatedPage(
            page2=transparency_page_object,
            tx=BORDER_SIZE,
            ty=BORDER_SIZE,
            expand=False
        )

        page_object.mergeTranslatedPage(
            page2=border_page_object,
            tx=slides_translation[number_of_slide_modulo]['x'],
            ty=slides_translation[number_of_slide_modulo]['y'],
            expand=False
        )

        if (
            number_of_slide_modulo == (N - 1)
        ) or (
            number_of_slide == (number_of_slides - 1)
        ):
            n_up_writer.add_page(page_object)

    n_up_writer.write(n_up_object)
    return n_up_object


def export_pdf_metadata(input_file_path, metadata_file_path):
    completed_process = subprocess.run(
        [
            'pdftk',
            input_file_path,
            'dump_data_utf8',
            'output',
            metadata_file_path
        ],
        stdout=subprocess.PIPE
    )
    if completed_process.returncode == 0:
        return True, metadata_file_path
    else:
        return False, completed_process.stdout.decode('utf-8').strip()


def convert_pdf_to_grayscale(input_file_path, output_file_path):
    completed_process = subprocess.run(
        [
            'gs',
            '-sDEVICE=pdfwrite',
            '-sColorConversionStrategy=Gray',
            '-dProcessColorModel=/DeviceGray',
            '-dCompatibilityLevel=1.4',
            '-dAutoRotatePages=/None',
            '-dEmbedAllFonts=true',
            '-dCompressFonts=false',
            '-dSubsetFonts=false',
            '-dNOPAUSE',
            '-dBATCH',
            '-sOutputFile={}'.format(output_file_path),
            input_file_path
        ],
        stdout=subprocess.PIPE
    )
    if completed_process.returncode == 0:
        return True, output_file_path
    else:
        return False, completed_process.stdout.decode('utf-8').strip()


def split_pdf_to_pages(input_file_path, output_directory_path):
    completed_process = subprocess.run(
        [
            'pdftk',
            input_file_path,
            'burst',
            'output',
            os.path.join(output_directory_path, 'pg_%04d.pdf')
        ],
        stdout=subprocess.PIPE
    )
    if completed_process.returncode == 0:
        return True, output_directory_path
    else:
        return False, completed_process.stdout.decode('utf-8').strip()


def join_pages_to_pdf(input_directory_path, output_file_path):
    completed_process = subprocess.run(
        [
            'pdftk',
            *[os.path.join(input_directory_path, page) for page in sorted(
                [file_name for file_name in os.listdir(
                    input_directory_path
                ) if file_name.startswith('pg_')]
            )],
            'cat',
            'output',
            output_file_path
        ],
        stdout=subprocess.PIPE
    )
    if completed_process.returncode == 0:
        return True, output_file_path
    else:
        return False, completed_process.stdout.decode('utf-8').strip()


def import_pdf_metadata(
    input_file_path,
    metadata_file_path,
    output_file_path
):
    completed_process = subprocess.run(
        [
            'pdftk',
            input_file_path,
            'update_info_utf8',
            metadata_file_path,
            'output',
            output_file_path
        ],
        stdout=subprocess.PIPE
    )
    if completed_process.returncode == 0:
        return True, output_file_path
    else:
        return False, completed_process.stdout.decode('utf-8').strip()


def get_ink_coverage(input_file_path, ink_coverage_file_path):
    completed_process = subprocess.run(
        [
            'gs',
            '-sDEVICE=inkcov',
            '-o',
            ink_coverage_file_path,
            input_file_path
        ],
        stdout=subprocess.PIPE
    )
    if completed_process.returncode == 0:
        return True, ink_coverage_file_path
    else:
        return False, completed_process.stdout.decode('utf-8').strip()


def convert_eps_to_pdf(input_file_path, output_file_path):
    completed_process = subprocess.run(
        [
            'epstopdf',
            input_file_path,
            output_file_path
        ],
        stdout=subprocess.PIPE
    )
    if completed_process.returncode == 0:
        return True, output_file_path
    else:
        return False, completed_process.stdout.decode('utf-8').strip()


def convert_pdf_to_ppm(input_file_path, dpi, output_file_path):
    completed_process = subprocess.run(
        [
            'pdftoppm',
            '-singlefile',
            '-cropbox',
            '-r',
            str(dpi),
            input_file_path,
        ],
        stdout=subprocess.PIPE
    )
    if completed_process.returncode == 0:
        with open(output_file_path, 'wb') as output_file:
            output_file.write(completed_process.stdout)
        return True, output_file_path
    else:
        return False, completed_process.stdout.decode('utf-8').strip()


def convert_ppm_to_pdf(input_file_path, dpi, output_file_path):
    completed_process = subprocess.run(
        [
            'xvfb-run',
            'gimp',
            '--new-instance',
            '--no-interface',
            '--no-data',
            '--no-fonts',
            '--batch-interpreter',
            'python-fu-eval',
            '-b',
            "image = pdb.gimp_file_load('{input_file_path}', '{input_file_path}'); pdb.gimp_image_set_resolution(image, {dpi}, {dpi}); drawable = pdb.gimp_image_get_active_drawable(image); pdb.file_pdf_save(image, drawable, '{output_file_path}', '{output_file_path}', True, True, True); pdb.gimp_quit(1)".format(input_file_path=input_file_path, dpi=str(dpi), output_file_path=output_file_path),
        ],
        stdout=subprocess.PIPE
    )
    if completed_process.returncode == 0:
        return True, output_file_path
    else:
        return False, completed_process.stdout.decode('utf-8').strip()



def open_image(image_path):
    """
    Input:
    * ``: () ...

    Output:
    * () ...

    Function:
    * ...
    """

    try:
        return Image.open(image_path)
    except:
        return None


def save_image(image_object, image_path):
    """
    Input:
    * ``: () ...

    Output:
    * () ...

    Function:
    * ...
    """

    try:
        image_object.save(image_path)
        return image_object
    except:
        return None


def get_image_exif(
    image_object,
    exif_config=config.EXIF_CONFIG
):
    '''
    Input:
    * ``: () ...

    Output:
    * () ...

    Function:
    * ...
    '''

    exif = {}

    exif_raw = image_object._getexif()

    if exif_raw is None:
        return exif

    for (key, config) in exif_config.items():
        value_raw = exif_raw.get(config['tag'])
        if value_raw is not None:
            value = config['value'](value_raw)
            if value is not None:
                exif[key] = value
    return exif


def normalize_image_orientation(
    image_object
):
    """
    Input:
    * ``: () ...

    Output:
    * () ...

    Function:
    * ...
    """

    image_orientation = get_image_exif(
        image_object,
        {
            'Orientation': {
                'tag': 274,
                'value': lambda x: x
            }
        }
    ).get("Orientation", 1)
    if image_orientation == 2:
        return image_object.transpose(
            Image.FLIP_LEFT_RIGHT
        )
    elif image_orientation == 3:
        return image_object.transpose(
            Image.ROTATE_180
        )
    elif image_orientation == 4:
        return image_object.transpose(
            Image.ROTATE_180
        ).transpose(
            Image.FLIP_LEFT_RIGHT
        )
    elif image_orientation == 5:
        return image_object.transpose(
            Image.ROTATE_270
        ).transpose(
            Image.FLIP_LEFT_RIGHT
        )
    elif image_orientation == 6:
        return image_object.transpose(
            Image.ROTATE_270
        )
    elif image_orientation == 7:
        return image_object.transpose(
            Image.ROTATE_90
        ).transpose(
            Image.FLIP_LEFT_RIGHT
        )
    elif image_orientation == 8:
        return image_object.transpose(
            Image.ROTATE_90
        )
    else:
        return image_object


def get_image_size(image_object):
    """
    Input:
    * ``: () ...

    Output:
    * () ...

    Function:
    * ...
    """
    
    return image_object.size


def resize_image(
    image_object,
    resized_size,
    fit='fill',
    scale_up=True
):
    """
    Input:
    * ``: () ...

    Output:
    * () ...

    Function:
    * ...
    """

    image_width, image_height = get_image_size(image_object)
    image_ratio = image_width / image_height

    resized_width, resized_height = resized_size
    if not scale_up:
        resized_width = min(resized_width, image_width)
        resized_height = min(resized_height, image_height)
    resized_ratio = resized_width / resized_height

    if fit == 'fill':
        return image_object.resize(
            (int(resized_width), int(resized_height)),
            Image.LANCZOS
        )

    elif fit == 'contain':
        if image_ratio > resized_ratio:
            # resized_width = resized_width
            resized_height = resized_width / image_ratio
        elif image_ratio < resized_ratio:
            resized_width = resized_height * image_ratio
            # resized_height = resized_height
        # elif image_ration == resized_ratio:
            # resized_width = resized_width
            # resized_height = resized_height
        return image_object.resize(
            (int(resized_width), int(resized_height)),
            Image.LANCZOS
        )

    elif fit == 'cover':
        if image_ratio > resized_ratio:
            tmp_width = resized_height * image_ratio
            tmp_height = resized_height
            tmp_image = resize_image(
                image_object,
                (tmp_width, tmp_height),
                fit='contain'
            )
            tmp_image_width, tmp_image_height = get_image_size(tmp_image)
            return tmp_image.crop(map(int, (
                (tmp_image_width-resized_width)/2,
                0,
                resized_width + ((tmp_image_width-resized_width)/2),
                tmp_image_height
            )))
        elif image_ratio < resized_ratio:
            tmp_width = resized_width
            tmp_height = resized_width / image_ratio
            tmp_image = resize_image(
                image_object,
                (tmp_width, tmp_height),
                fit='contain'
            )
            tmp_image_width, tmp_image_height = get_image_size(tmp_image)
            return tmp_image.crop(map(int, (
                0,
                (tmp_image_height-resized_height)/2,
                tmp_image_width,
                resized_height + ((tmp_image_height-resized_height)/2),
            )))
        elif image_ratio == resized_ratio:
            return resize_image(
                image_object,
                (resized_width, resized_height),
                fit='contain'
            )

    else:
        return image_object


def convert_color_profile_to_sRGB(image_object):
    """
    Input:
    * ``: () ...

    Output:
    * () ...

    Function:
    * ...
    """

    original_color_profile_bytes = image_object.info.get("icc_profile")
    if original_color_profile_bytes is None:
        return image_object
    else:
        try:
            original_color_profile = ImageCms.ImageCmsProfile(
                io.BytesIO(original_color_profile_bytes)
            )
            srgb_color_profile = ImageCms.createProfile("sRGB")
            return ImageCms.profileToProfile(
                image_object,
                original_color_profile,
                srgb_color_profile
            )
        except:
            return image_object
