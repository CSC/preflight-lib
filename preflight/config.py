def _get_gps_latitude(gps):
    return None

  #  try:
  #      degrees, minutes, seconds = map(
  #          lambda x: x[0]/x[1],
  #          gps[2]
  #      )
  #      latitude = degrees + (minutes/60) + (seconds/3600)
  #      reference = gps[1]
  #      if reference != 'N':
  #          latitude = 0 - latitude
  #      return str(latitude)
  #  except KeyError:
  #      return None


def _get_gps_longitude(gps):
    return None

   # try:
   #     degrees, minutes, seconds = map(
   #         lambda x: x[0]/x[1],
   #         gps[4]
   #     )
   #     longitude = degrees + (minutes/60) + (seconds/3600)
   #     reference = gps[3]
   #     if reference != 'E':
   #         longitude = 0 - longitude
   #     return str(longitude)
   # except KeyError:
   #     return None


EXIF_CONFIG = {
    'Description': {
        'tag': 270,
        'value': lambda x: None if not x else x
    },
    'Camera Make': {
        'tag': 271,
        'value': lambda x: None if not x else x
    },
    'Camera Model': {
        'tag': 272,
        'value': lambda x: None if not x else x
    },
    'Orientation': {
        'tag': 274,
        'value': lambda x: None if not x else x
    },
    'Software': {
        'tag': 305,
        'value': lambda x: None if not x else x
    },
    'DateTime': {
        'tag': 306,
        'value': lambda x: None if not x else x
    },
    'Artist': {
        'tag': 315,
        'value': lambda x: None if not x else x
    },
    'Copyright': {
        'tag': 33432,
        'value': lambda x: None if not x else x
    },
  # PIL(low) problem - to be investigated...
  #  'Exposure': {
  #      'tag': 33434,
  #      'value': lambda x: None if not x else '{0}/{1} seconds'.format(
  #          *map(str, x)
  #      )
  #  },
  #    'Aperture': {
  #        'tag': 33437,
  #        'value': lambda x: None if not x else 'f/{0}'.format(
  #            str(x[0]/x[1])
  #        )
  #    },
    'GPS Latitude': {
        'tag': 34853,
        'value': _get_gps_latitude
    },
    'GPS Longitude': {
        'tag': 34853,
        'value': _get_gps_longitude
    },
    'ISO': {
        'tag': 34855,
        'value': lambda x: None if not x else str(x)
    },
  #  'Lens Focal Length': {
  #      'tag': 37386,
  #      'value': lambda x: None if not x else str(x[0]/x[1])
  #  },
    'Camera Owner': {
        'tag': 42032,
        'value': lambda x: None if not x else x
    },
    'Lens Model': {
        'tag': 42036,
        'value': lambda x: None if not x else x
    },
}
